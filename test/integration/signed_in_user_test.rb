require 'test_helper'

class SignedInUserTest < ActionDispatch::IntegrationTest

  def setup # sign_in_user
    @jim = users(:jim)
    post_via_redirect "/users/sign_in", "user[email]" => @jim.email, "user[password]" => 'password'
  end

  test "signed into site" do
    assert_equal '/', path
    assert_equal 'Signed in successfully.', flash[:notice]
  end

  test "signed in user can create posts" do
    #new post page
    get "/posts/new"
    assert_equal  "/posts/new", path
    assert_select 'h1', 'New Post'
    assert_select "form"

    valid_content = "hello to the world"
    create_post(valid_content)
    assert_select 'p', valid_content
  end

  test "cannot post invalid words" do
    invalid_content = "inscrutable antonymns"
    get "/posts/new"
    post_via_redirect "/posts", "post"=>{"content"=>invalid_content}, "commit"=>"Create Post"
    assert_equal  "/posts", path
    assert_select "div", "Please review the problems below:"
  end

  test "user can modify own posts" do
    create_post("to be")
    assert_select 'a', 'Edit' #edit link
    path_post = path
    get "#{path_post}/edit"
    assert_select 'h1', 'Editing Post'
    assert_select "form"

    new_content = "not hello."
    update_post(new_content, path_post)
    assert_equal path_post, path
    # assert_select 'p', new_content #
    assert_match new_content, response.body
  end

  test "cannot modify posts of other users" do
    post3 = posts(:three)

    get_via_redirect edit_post_path(post3)
    assert_equal root_path, path

    update_post "the world", post_path(post3)
    assert_equal root_path, path

    #content not modified
    get post_path(post3)
    assert_select 'p', 'to be or not'
  end

  private

    def create_post(content)
      post_via_redirect "/posts", "post"=>{"content"=>content}, "commit"=>"Create Post"
    end

    def update_post(content, path_post)
      patch_via_redirect path_post, "post"=>{"content"=> content}, "commit"=>"Update Post"
    end

end